# RxJS Samples

Examples of RxJS use

### Version
This uses RxJS version 6 - [https://github.com/ReactiveX/rxjs](https://github.com/ReactiveX/rxjs)


### Installation

Requires [Node.js](https://nodejs.org/) v4+ to run.

Install dependencies

```sh
$ npm install
```

### Run

```sh
$ npm start
```

Open [http://localhost:8080/webpack-dev-server/](http://localhost:8080/webpack-dev-server/)


----
Based on [rxjs_boiler](https://github.com/bradtraversy/rxjs_boiler)