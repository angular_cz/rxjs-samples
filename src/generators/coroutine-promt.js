// NODE Only

const Promise = require('bluebird');
const prompt = require('prompt-promise');

const meowNames = require('./meow-names.js').all;

const co = require('co');

function getPossibleWordsFor(word) {
  const firstLetter = word.substr(-1).toUpperCase();
  return meowNames.filter(name => name.startsWith(firstLetter))
}

function getRandomFrom(array) {
  const index = Math.floor((Math.random() * array.length));
  return array[index]
}

const memory = new Set();

function checkRules(previous, current) {
  previous = previous.toLowerCase();
  current = current.toLowerCase();
  const lastLetter = previous.substr(-1);
  if (!current.startsWith(lastLetter)) {
    throw new Error(`${current} does not with ${lastLetter} (last letter from ${previous})`);
  }

  if (memory.has(current)) {
    throw new Error(`${current} had already been used...`);
  }
  memory.add(current);
}

co(function*() {
  console.log('Start with:');
  let responseWord = yield Promise.resolve(getRandomFrom(meowNames));
  console.log('Computer starts with: %s', responseWord);

  while (true) {
    let userWord = yield prompt('name: ');

    console.log('User: %s', userWord);
    checkRules(responseWord, userWord);

    responseWord = yield Promise.delay(500).then(() => {
      const availableNames = getPossibleWordsFor(userWord);
      return getRandomFrom(availableNames)
    });

    console.log('Computer: %s', responseWord);
    checkRules(userWord, responseWord);
  }
}).catch(function(e) {
  console.error(e.message);
  process.exit();
});
