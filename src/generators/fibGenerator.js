function *fib() {
  let a = 0;
  let b = 1;

  yield a;
  yield b;

  while (true) {
    [a, b] = [b, a + b];

    yield b;
  }
}

function *filter(gen, condition) {
  let value;

  while (true) {
    value = gen.next().value;

    if(condition(value)) {
      yield value;
    }
  }

}

function *takeWhileGen(gen, condition) {

  let value = gen.next().value;

  while(condition(value)) {
    yield value;
    value = gen.next().value;
  }
}

function isEven(number) {
  return number % 2 === 0
}

let g = fib();
let evenGen = filter(g, isEven);
let takeWhile = takeWhileGen(evenGen, value => value < 4000000);

let evenFibs = [...takeWhile];
let sum = evenFibs.reduce((sum, value) => sum + value, 0);

console.log(sum);



// 4613732