const Promise = require('bluebird');

function continueWithValue(gInstance, oldValue) {
  const {value, done} = gInstance.next(oldValue);
  if (done) {
    console.log('done', value);
  } else if (value instanceof Promise) {
    value.then(value => continueWithValue(gInstance, value))
  } else {
    continueWithValue(gInstance, value)
  }
}

function co(g) {
  const gInstance = g();

  continueWithValue(gInstance)

}


co(function*() {
  let n1 = yield Promise.delay(1000).then(() => 11);
  console.log('point 1');
  let n2 = yield Promise.delay(1000).then(() => 12);
  console.log('point 2');

  return n1 + n2;
});