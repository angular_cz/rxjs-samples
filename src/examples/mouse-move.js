import {fromEvent, of} from 'rxjs';
import {map, throttleTime, pairwise, scan} from 'rxjs/operators';

const calculateDistance = (point1, point2) => {
    const dx = point2.x - point1.x;
    const dy = point2.y - point1.y;

    return Math.hypot(dx, dy);
};

export const mouseMove$ = fromEvent(document, 'mousemove');


export const mouseDistance$ = mouseMove$
    .pipe(
        map(event => ({
            x: event.clientX,
            y: event.clientY
        })),
        throttleTime(100),
        pairwise(),
        scan((distance, pair) => {
            let currentDistance = calculateDistance(pair[0], pair[1]);

            return distance + currentDistance;
        }, 0)
    );

const distanceElement = document.querySelector('#mouse-distance');
mouseDistance$.subscribe((distance) => {
    distanceElement.innerHTML = `Ujeli jste ${distance.toLocaleString()}px`;
});