import {fromEvent, interval} from 'rxjs';
import {map, share, refCount, publish, sample} from 'rxjs/operators';

const dateToTime = date => `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}:${date.getMilliseconds()}`;

const counterTake = document.querySelector('#counter .take');
const counterValue = document.querySelector('#counter .value');
const counterTaken = document.querySelector('#counter .taken');

const interval$ = interval(100)
    .pipe(
        map(() => {
                //console.log('time');
                return new Date()
            }
        ),
        share()
    );

interval$
    .subscribe((date) => {
        counterValue.innerHTML = dateToTime(date)
    });

const take$ = fromEvent(counterTake, 'click');
interval$.pipe(sample(take$))
    .subscribe((date) => {
        counterTaken.innerHTML += dateToTime(date) + '<br />'
    });