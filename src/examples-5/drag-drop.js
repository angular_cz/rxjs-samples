import Observable from 'rxjs/Rx';

let square = document.querySelector('.square');

const moveSquareByCoordinates = ({x, y}) => {
  const style = getComputedStyle(square);
  const top = style.getPropertyValue('top');
  const left = style.getPropertyValue('left');

  square.style.left = `${parseInt(left) + x}px`;
  square.style.top = `${parseInt(top) + y}px`;
};

const mouseDown$ = Observable.fromEvent(square, 'mousedown');
const mouseMove$ = Observable.fromEvent(square, 'mousemove');
const mouseUp$ = Observable.fromEvent(square, 'mouseup');

const stop$ = Observable.merge(
    Observable.fromEvent(square, 'mouseup'),
    Observable.fromEvent(square, 'mouseout')
);

const windows = mouseMove$
    .map(event => ({
      x: event.clientX,
      y: event.clientY
    }))
    .pairwise()
    .map(([p1, p2]) => ({
          x: p2.x - p1.x,
          y: p2.y - p1.y
        })
    )
    .windowToggle(mouseDown$, (i) => stop$)
    .mergeAll();

windows
    .subscribe(moveSquareByCoordinates);