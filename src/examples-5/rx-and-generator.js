import Observable from 'rxjs/Rx';

export const mousePositions$ = Observable.fromEvent(document, 'mousemove')
    .map(event => ({
      x: event.clientX,
      y: event.clientY
    }))
    .throttleTime(100);

function* mousePositionSamples(n = 5) {
  let samples = new Set();

  for (n; n > 0; n--) {
    const sample = yield;
    samples.add(sample);
  }

  return samples.values();
}

let m = mousePositionSamples(5);
m.next();

const subscribtion = mousePositions$
    .subscribe({
      next: sample => {
        console.log('next', sample);

        const {value, done} = m.next(sample);

        if (done) {
          console.log('done', value);

          subscribtion.unsubscribe();
        }
      }
    });