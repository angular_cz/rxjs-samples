import Observable from 'rxjs/Rx';

const dateToTime = date => `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}:${date.getMilliseconds()}`;

const counterTake = document.querySelector('#counter .take');
const counterValue = document.querySelector('#counter .value');
const counterTaken = document.querySelector('#counter .taken');

const interval$ = Observable.interval(100)
    .map(() => {
      //console.log('time');

      return new Date()
    })
    .share();

interval$
    .subscribe((date) => {
      counterValue.innerHTML = dateToTime(date)
    });

const take$ = Observable.fromEvent(counterTake, 'click');
interval$.sample(take$)
    .subscribe((date) => {
      counterTaken.innerHTML += dateToTime(date) + '<br />'
    });