module.exports = {
	entry: ['babel-polyfill', './src/app.js'],
	output: {
		filename:'bundle.js'
	},
  watch: true,
	module: {
		loaders: [{
			exclude: '/node_modules/',
			loader: 'babel-loader',
			query: {
				presets: ['es2015']
			}
		}]
	}
};